from pages.books_page import BooksPage
from tests.books_base import TestTemplate

class TestForumPage(TestTemplate):

    BOOKS_ON_PAGE = 12
    BOOKS_UNDER_100 = 3
    BOOKS_ABOVE_500 = 3

    #Tests if all 12 books are visible on site
    def test_all_books_visible(self):
        m =BooksPage(self.driver)
        assert m.all_books_count() == self.BOOKS_ON_PAGE

    #Tests filtering by max price
    def test_max_price_filter(self):
        m = BooksPage(self.driver)
        m.filter_by_max_price()
        assert m.all_books_count() == self.BOOKS_UNDER_100

    # Tests filtering by min price
    def test_min_price_filter(self):
        m = BooksPage(self.driver)
        m.filter_by_min_price()
        assert m.all_books_count() == self.BOOKS_ABOVE_500


    #Tests if prices is correctly sorted in ascending way
    def test_sort_price_asc(self):
        m = BooksPage(self.driver)
        m.sort_by_price_asc()
        price = m.books_prices()
        assert price == sorted(price)

    # Tests if prices is correctly sorted in ascending way
    def test_sort_price_desc(self):
        m = BooksPage(self.driver)
        m.sort_by_price_desc()
        price = m.books_prices()
        assert price == sorted(price, reverse=True)

    # Tests if filtering by "helion" producer works properly by checking if producer of
    # every book visible on page is "helion"
    def test_helion_producer_filter(self):
        m = BooksPage(self.driver)
        m.filter_by_helion_producer()
        helion = m.helion_books()
        assert helion == "Helion"