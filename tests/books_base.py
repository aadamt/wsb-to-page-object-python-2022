import pytest
from selenium import webdriver
from selenium.webdriver.firefox.service import Service as FirefoxService
from webdriver_manager.firefox import GeckoDriverManager

HTML = "https://www.sklepdemo.pl/ksiazki-naukowe-cat-7"

class TestTemplate:
    @pytest.fixture(autouse=True)
    def setUp(self):
        self.service = FirefoxService(executable_path=GeckoDriverManager().install())
        self.driver = webdriver.Firefox(service=self.service)
        self.driver.get(HTML)
        #self.driver.maximize_window()
        self.driver.implicitly_wait(5)

        yield self.driver
