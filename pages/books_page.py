import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.firefox.service import Service as FirefoxService
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class BooksPage():

    book_price = "product-price "
    max_price = "#filter-price2"
    min_price = "#filter-price1"
    filter_enabled = ".single-pill > span:nth-child(1) > b:nth-child(1)"
    helion_filter_enabled = ".single-pill > span:nth-child(1)"


    sorter_button = ".sorter-button"
    price_desc = ".sorter-listing > span:nth-child(4) > label:nth-child(2)"
    price_asc = ".sorter-listing > span:nth-child(3) > label:nth-child(2)"

    helion_filtr = "div.gs-filtr-container:nth-child(7) > div:nth-child(2) > ul:nth-child(1) > li:nth-child(1) > label:nth-child(2)"
    helion_title = "div.product:nth-child({}) > a:nth-child(4) > em:nth-child(3) > strong:nth-child(1)"

    def __init__(self, driver):
        self.driver = driver

    def all_books_count(self):
        return len(self.driver.find_elements(By.CLASS_NAME, self.book_price))

    def filter_by_max_price(self):
        max_price = self.driver.find_element(By.CSS_SELECTOR,self.max_price)
        max_price.clear()
        max_price.send_keys("100")
        element = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, self.filter_enabled))
        )

    def filter_by_min_price(self):
        max_price = self.driver.find_element(By.CSS_SELECTOR,self.min_price)
        max_price.clear()
        max_price.send_keys("500")
        element = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, self.filter_enabled))
        )
    def filter_by_helion_producer(self):
        filtr = self.driver.find_element(By.CSS_SELECTOR, self.helion_filtr).click()
        element = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, self.helion_filter_enabled))
        )
    def helion_books(self):
        all_books_count = len(self.driver.find_elements(By.CLASS_NAME, self.book_price))
        for book in range(all_books_count):
            helion = self.driver.find_element(By.CSS_SELECTOR,"div.product:nth-child({}) > a:nth-child(4) > em:nth-child(3) > strong:nth-child(1)".format(all_books_count))
        return helion.get_attribute('innerHTML')

    def sort_by_price_asc(self):
        self.driver.find_element(By.CSS_SELECTOR,self.sorter_button).click()
        self.driver.find_element(By.CSS_SELECTOR,self.price_asc).click()

    def sort_by_price_desc(self):
        self.driver.find_element(By.CSS_SELECTOR, self.sorter_button).click()
        self.driver.find_element(By.CSS_SELECTOR, self.price_desc).click()

    def books_prices(self):
        prices = self.driver.find_elements(By.CLASS_NAME, self.book_price)
        list =[]
        #Loop for adding only discounted price if there are both discounted and normal
        for price in prices:
            discounted = price.find_element(By.TAG_NAME,"b")
            list.append(discounted.text)
        #Loop for replacing string values to float values
        new_list = []
        for i in list:
            x = i.replace(",",".")
            new_list.append(float(x))
        #returns list of prices in order they appear on site
        return new_list